//Help from https://brennan.io/2015/01/16/write-a-shell-in-c/
//Help from http://www.microhowto.info/howto/capture_the_output_of_a_child_process_in_c.html
//Help from https://stackoverflow.com/questions/12711799/creating-child-processes-in-parallel
//Help from https://www.geeksforgeeks.org/remove-spaces-from-a-given-string/

//Susanna Järventausta
//Opnro 0542325

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

#define BUFSIZE 500
#define LSH_TOK_DELIM " \t\r\n\a"

void loop(char **parameters);
char *getCommand();
void exitWish(char **args, char **paths, char *command);
char **parseCommand(char* command, char** args);
char **builtInPath(char **args, char **paths);
int execute(char **args, char **paths);
void builtInCd(char **args);
int numberOfArguments(char **args);
int executeParallel(char **args, char **paths);
void launchParallel(char *command, char **paths);
int launch(char **args, char **paths, char *destination);
char *insertPathToCommand(char **paths, char *command);
void makeDup(char *destination);
void removeSpace(char *string);


int main(int argc, char **args){
	char error_message[45] = "Too many commandline parameters, exiting\n";

	//Too many commandline parameters
	if(numberOfArguments(args)>2){
		write(STDERR_FILENO, error_message, strlen(error_message));
		exit(1);}

	loop(args);	
	return 0;
}

//Loops until exit
void loop(char **parameters){
	char *command, error_message[30] = "Malloc failed, exiting\n", error_message2[40] = "Couldn't open file, exiting\n";
  	char **args, **paths;
  	int status = 1, getLine = 0;
	FILE *file;
	size_t bufsize = 0;

	if(!(paths = (char**)malloc(100))){
		write(STDERR_FILENO, error_message, strlen(error_message));
		exit(1);}

	//Initial path
	paths[0] = "/bin";
	paths[1] = NULL;

	//Open possible batch file
	if(parameters[1] != NULL){
		if((file = fopen(parameters[1], "r")) == NULL){
			write(STDERR_FILENO, error_message2, strlen(error_message));
			exit(1);}}

	do{	//Get user's or file's command
		if(parameters[1] == NULL){
			command = getCommand();
		}else{
			getLine = getline(&command, &bufsize, file);}
		if(getLine == EOF){
			if(parameters[1] != NULL){fclose(file);}
	      		exitWish(args, paths, command);}

		//Parse the command if no parallel or redirection
		args = (char**)malloc(100);
		if(strstr(command, "&") != NULL || strstr(command, ">")!=NULL){args[0]=command;}
    		else{
			args = parseCommand(command, args);
			if(args[0] == NULL){continue;}}
	
		//Create new paths given by user
		if (strcmp(args[0], "path") == 0){
			paths = builtInPath(args, paths);
			continue;
		//Exit loop
		}else if (strcmp(args[0], "exit") == 0) {
			if(parameters[1] != NULL){fclose(file);}
	      		exitWish(args, paths, command);
		}else{
			//Execute the command
    			status = execute(args, paths);}

		free(args);
		free(command);
    		
	}while (status);
}

//Returns user's command from stdin
char *getCommand(){

	char *command = NULL;
  	size_t bufsize = 0;
	printf("wish> ");
  	getline(&command, &bufsize, stdin);

  	return command;
}

//Safe exit
void exitWish(char **args, char **paths, char *command) {

    	free(args);
	free(paths);
	free(command);
	exit(0);
}

//Returns user's command as a list
char **parseCommand(char* command, char** args){

	int i = 0;
	char *token;

	//Divide command to pieces by " \t\r\n\a"
	token = strtok(command, LSH_TOK_DELIM);

	while (token != NULL) {
		args[i] = token;
		i++;
		token = strtok(NULL, LSH_TOK_DELIM);}

	free(token);
	args[i] = NULL;
	return args;
}

//Makes new path array with user's parameters
char **builtInPath(char **args, char **paths){
	int i = 1;

	if(numberOfArguments(args) == 1){ //"path"
		free(paths);
		paths = (char**)malloc(100);
		paths[0] = NULL;
	}else{ //"path", "xx", ...
		free(paths);
		paths = (char**)malloc(100);
		
		for(i=1; i < numberOfArguments(args); i++){
			paths[i-1] = args[i];}
			//paths[i-1] = args[i];}
		//strcpy(paths[i], NULL);
		paths[i] = NULL;
	}
	return paths;
}

int execute(char **args, char **paths){
	char *destination, *command, **destinations;
	char *error_message = "Redirection failed, correct use: [command] > [file] or [command]>[file]\n";

	//Change directory
	if (strcmp(args[0], "cd") == 0){
		builtInCd(args);

	//Execute a non-built-in-command
	}else{
		//Parallel execution
		if(strstr(args[0], "&") !=NULL){
			return executeParallel(args, paths);

		//Split string to command and destination file and parse command
		}else if(strstr(args[0], ">")!=NULL){

			command = strtok(args[0], ">");
			destination = strtok(NULL, ">");
			//printf("Destination:%s\n", destination);

			args = parseCommand(command, args);
			destinations = (char**)malloc(100);
	
			//Check syntax of redirection
			if(numberOfArguments(parseCommand(destination, destinations))!=1){
				write(STDERR_FILENO, error_message, strlen(error_message));
				free(destinations);
				return 1;}
			free(destinations);
			removeSpace(destination);

		//No destination file		
		}else{
			destination = "stdout";}

		//Normal execution
		return launch(args, paths, destination);}
	return 1;
}

//Changes current directory
void builtInCd(char **args){
	char error_message[30] = "Changing the directory failed\n", error_message2[40] = "Cd failed, correct use: cd [directory]\n";
	
	if(numberOfArguments(args) == 2){ //"cd", "folder"
		if(chdir(args[1]) == -1){
			write(STDERR_FILENO, error_message, strlen(error_message));}
	}else{
		write(STDERR_FILENO, error_message2, strlen(error_message2));}
}

//Returns amount of items in an array before NULL
int numberOfArguments(char **args){
	int i = 0;
	char *word;
	word = args[i];
	while (word != NULL) {
		i++;
		word = args[i];}
	return i;
}

//Fork all commands to execute them in parallel mode
int executeParallel(char **args, char **paths){

	char *command, **commands = (char**)malloc(100);
	int i=-1, j = 0;
	pid_t pid[20];
	int status;

	//Get all different commands in an array
	command = strtok(args[0], "&");
	while(command != NULL){	
		i++;
		commands[i] = command;
		command = strtok(NULL, "&");}	

	//Fork a child for all commands
	while(j<=i){
		//printf("Gonna execute");
		pid[j]=fork();
		if(pid[j]<0){
			write(STDERR_FILENO, "Fork failed\n", strlen("Fork failed\n"));
			return -1;
		}else if(pid[j] == 0){
			launchParallel(commands[j], paths);
		}else{
			j++;}
	}
	//Wait for children to stop
	while(wait(&status)>0){}

	free(commands);
	return 1;
}

//Parse command and execute
void launchParallel(char *command, char **paths){
	char *destination, **destinations, **args = (char**)malloc(100);
	char *commandwPath = NULL, *error_message = "Redirection failed, correct use: [command] > [file] or [command]>[file]\n";

	//Parse command with redirection
	if(strstr(command, ">")!=NULL){

		commandwPath = strtok(command, ">");
		destination = strtok(NULL, ">");
		args = parseCommand(commandwPath, args);
		destinations = (char**)malloc(100);
	
		//Check syntax of redirection
		if(numberOfArguments(parseCommand(destination, destinations))!=1){
			write(STDERR_FILENO, error_message, strlen(error_message));
			exit(0);}
		free(destinations);
		removeSpace(destination);

	//Parse command without redirection
	}else{
		commandwPath = command;
		args = parseCommand(commandwPath, args);
		destination = "stdout";}

	commandwPath= insertPathToCommand(paths, args[0]);

	//Readirrect output if wanted
	if(strcmp(destination, "stdout") != 0){
		makeDup(destination);}
		
	if(execv(commandwPath, args) == -1) {
		write(STDERR_FILENO, "Command was not executable\n", strlen("Command was not executable\n"));
		exit(0);}
}

//Forks a child process that executes a non-built-in command
int launch(char **args, char **paths, char *destination){
	pid_t pid;
	int status; 
	char *command;

	//Fork a child process
	pid = fork();
	if(pid < 0){ //Fork failed
		write(STDERR_FILENO, "Fork failed\n", strlen("Fork failed\n"));
	
	}else if(pid == 0) { //Child process goes here
		command = insertPathToCommand(paths, args[0]);
		setpgid(0, 0);

		//Readirrect output if wanted
		if(strcmp(destination, "stdout") != 0){
			makeDup(destination);}
		
		if(execv(command, args) == -1) {
			write(STDERR_FILENO, "Command was not executable\n", strlen("Command was not executable\n"));
			exit(0);}

	}else{ //Parent process goes here
		//Wait till child is done
		if(wait(&status) == -1){
			write(STDERR_FILENO, "Wait in parent failed\n", strlen("Wait in parent failed\n"));}
	}
	return 1;
}

//If command is executable with a path return them together
//If not return normal command
char *insertPathToCommand(char **paths, char *command){

	int i=0;
	char* tempCommand = (char *)malloc(100);

	//Go through all path possibilities
	while(paths[i] != NULL){

		strcpy(tempCommand, paths[i]);
		strcat(tempCommand, "/");
		strcat(tempCommand, command);
		//If executable
		if(access(tempCommand, X_OK)==0){
			command = tempCommand;
			break;}
		i++;
	}
	return command;
}

//Puts childs output to desitnation file
void makeDup(char *destination){
	int putputFile;

	if ((putputFile = creat(destination,  0644)) < 0){ //0644 = -rw-r--r--
		write(STDERR_FILENO, "Couldn't open output file\n", strlen("Command was not executable\n"));
		exit(0);}

	if (dup2(putputFile, STDOUT_FILENO) < 0){
		write(STDERR_FILENO, "Putting stdout of child to file failed\n", strlen("Command was not executable\n"));
		exit(0);}
			
	if (dup2(putputFile, STDERR_FILENO) < 0){
		write(STDERR_FILENO, "Putting stderror of child to file failed\n", strlen("Command was not executable\n"));
		exit(0);}

	close(putputFile);
}

//Removes spaces from a string
void removeSpace(char *string){

	int j = 0;

	// Go thorugh given string. If current character 
    	// is not space, then place it at index 'j++' 
    	for (int i = 0; string[i]; i++){ 
        	if (string[i] != ' '){ 
            		string[j++] = string[i]; }
	}
                                   
    	string[j] = '\0'; 
}











